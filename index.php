<?php 
  include_once 'fn.php';
  include_once 'config.php';
  if(session('user_id') != ''){
    header("location:dashboard.php");die;    
  } 
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include_once 'header.html'; ?>
</head>
<body class="hold-transition login-page">
  <div id="cover-spin"></div>
  <div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <a href="#" class="h1"><b>KAZE</b></a>
      </div>
      <div class="card-body">
        <p class="login-box-msg">Sign in to start your session</p>

        <form action="#" method="post">
          <div class="input-group mb-3">
            <input type="email" id="email" class="form-control" placeholder="Email">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-envelope"></span>
              </div>
            </div>
          </div>
          <div class="input-group mb-3">
            <input type="password" id="password" class="form-control" placeholder="Password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-md-12">
              <span id="login" class="btn btn-primary btn-block">Sign In</span>
            </div>
            <div class="col-12 col-md-12"></div>
            <!-- /.col -->
          </div>
        </form>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.login-box -->

  <?php include_once 'script.html'; ?>
</body>
</html>
