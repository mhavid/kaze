$(document).on('click', '#btn_save_user', function() {
    let name = $('#name').val();
    let email = $('#email').val();
    let password = $('#password').val();
    let repassword = $('#repassword').val();
    let role = $('#role').val();

    if(password !== repassword){
        errorAlert('Password not Match');
        return;
    }

    let check = cekEmpty(['name','email','password','role']);
    if (check != '') return;

    $("#cover-spin").show();
    $.ajax({
        type: "POST",
        url: "kaze_process.php",
        data: {
            op: "save_user",
            name : name,
            email : email,
            password : password,
            repassword : repassword,
            role : role
        },
        success: function(r) {
            $("#cover-spin").hide();
            if (r == "") {
                successAlert('Successfully');
                window.location.href = 'dashboard.php';
            } else {
                errorAlert(r);
            }
        }
    });
})

/** Update User */
$(document).on('click', '#btn_update_user', function() {
    let name = $('#name').val();
    let email = $('#email').val();
    let role = $('#role').val();
    let uid = $('#uid').val();
    let hash = $('#h').val();

    let check = cekEmpty(['name','email','role']);
    if (check != '') return;

    $("#cover-spin").show();
    $.ajax({
        type: "POST",
        url: "kaze_process.php",
        data: {
            op: "update_user",
            name : name,
            email : email,
            role : role,
            uid : uid,
            hash: hash
        },
        success: function(r) {
            $("#cover-spin").hide();
            if (r == "") {
                successAlert('Update Success');
            } else {
                errorAlert(r);
            }
        }
    });
})

/** Delete User */
$(document).on('click', '.delete_user', function(){
    var uid = $(this).attr('uid');
    var hash = $(this).attr('hash');
    $.confirm({
        title: 'Attention!',
        content: 'Are You sure to delete this user?',
        buttons: {
            confirm: {
                text: 'No',
                btnClass: 'btn-red',
                keys: ['esc','enter'],
                action: function(){}
            },
            somethingElse: {
                text: 'Yes',
                btnClass: 'btn-blue',
                action: function(){
                    $('.overlay').show();
                    $.ajax({
                        type: "POST",
                        url: "kaze_process.php",
                        data: {
                            op: "delete_user",
                            uid : uid,
                            hash : hash
                        },
                        success: function(r) {
                            $(".overlay").hide();
                            if(r == 'error'){
                                errorAlert(' !Errorr, Hash error');
                                return;
                            }
                            $('#table_user').html(r);
                            successAlert('User Deleted');
                            $(".datatable").DataTable({
                                "responsive": true,
                                "lengthChange": false,
                                "autoWidth": false,
                                "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
                            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
                        }
                    });
                }
            }
        }
    });
})

$(document).on('click', '#login', function(){
    let email = $('#email').val();
    let password = $('#password').val();

    let check = cekEmpty(['email','password']);
    if (check != '') return;

    $("#cover-spin").show();
    $.ajax({
        type: "POST",
        url: "kaze_process.php",
        data: {
            op: "login",
            email : email,
            password : password,
        },
        success: function(r) {
            $("#cover-spin").hide();
            if (r == "") {
                successAlert('Successfully');
                window.location.href = 'dashboard.php';
            } else {
                errorAlert(r);
            }
        }
    });
})

/**
 * 
 * @param {String} Arr 
 * @returns TRUE, FALSE
 */
function cekEmpty(Arr = []) {
    let result = '';
    for (let i = 0; i < Arr.length; i++) {
        if ($('#' + Arr[i]).val() == "") {
            let text = (typeof($('#' + Arr[i]).attr('text')) === 'undefined' || $('#' + Arr[i]).attr('text') == '') ?
                Arr[i] : $('#' + Arr[i]).attr('text');
            result = (result == '') ? text : `${result}, ${text}`;
        }
    }
    result = (result != '') ? `${result} cant be empty` : result;
    if (result != '') errorAlert(result);
    return result;
}

/**
 * 
 * @param {String} message
 * Error message 
 */
function errorAlert(message) {
    toastr.error(message);
    /*return Swal.fire({
        title: "Failed",
        text: message,
        type: 'error',
        confirmButtonText: 'Close',
        allowOutsideClick: false
    })
    */
}

/**
 * 
 * @param {String} message 
 */
function successAlert(message) {
    toastr.success(message);
    /*return Swal.fire({
        title: "Success",
        text: message,
        type: 'success',
        confirmButtonText: 'Close'
    })
    */
}