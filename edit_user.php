<?php 
  include_once 'config.php';
  include_once 'fn.php';
  include_once 'tables.php';
  isLogin();

  $user_id = (isset($_GET['uid'])) ? $_GET['uid'] : '';
  $hash = (isset($_GET['h'])) ? $_GET['h'] : '';

  if(encrypt('edit_user_id'.$user_id) !== $hash){
      echo 'Hash Error'; die;
  }

  $q = query("SELECT * FROM k_user WHERE k_user.user_id='$user_id'");

  $row = mysqli_fetch_array($q);
  $username = $row['username'];
  $email = $row['email'];
  $role = $row['role'];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once 'header.html'; ?>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <?php include_once 'sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                    </div>
                </div>
                <!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card">
                    <!-- /.card-header -->
                    <?php include_once 'loading.html'; ?>
                    <div class="card-body" id="card_dashboard">
                        <form>
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" text="Name" value="<?=$username?>" id="name"/>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" text="Email" value="<?=$email?>" id="email"/>
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select text="Role" id="role" class="form-control select2">
                                    <option value="">- Select Role -</option>
                                    <option <?=($role == 0) ? 'selected' : '';?> value="0">User</option>
                                    <option <?=($role == 1) ? 'selected' : '';?> value="1">Admin</option>
                                </select>
                                <input type="hidden" id="uid" value="<?=$user_id?>"/>
                                <input type="hidden" id="h" value="<?=$hash?>"/>
                            </div>
                            <div class="form-group">
                                <a href="dashboard.php">
                                    <span class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</span>
                                </a>
                                <span id="btn_update_user" class="btn btn-primary">Update</span>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include_once 'footer.php'; ?>
    </div>
    
    <?php include_once 'script.html'; ?>
</body>

</html>