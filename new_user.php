<?php 
  include_once 'config.php';
  include_once 'fn.php';
  isLogin();
  if(session('role') != 1){
      header('location:dashboard.php');die;
  }

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once 'header.html'; ?>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <?php include_once 'sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                    </div>
                </div>
                <!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card">
                    <!-- /.card-header -->
                    <?php include_once 'loading.html'; ?>
                    <div class="card-body" id="card_dashboard">
                        <form>
                            <div class="form-group">
                                <label>Name</label>
                                <input class="form-control" text="Name" value="" id="name">
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" text="Email" value="" id="email">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" text="Password" value="" id="password">
                            </div>
                            <div class="form-group">
                                <label>Repeat Password</label>
                                <input type="password" class="form-control" text="repassword" value="" id="repassword">
                            </div>
                            <div class="form-group">
                                <label>Role</label>
                                <select text="Role" id="role" class="form-control select2">
                                    <option value="">- Select Role -</option>
                                    <option value="0">User</option>
                                    <option value="1">Admin</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <a href="dashboard.php">
                                    <span class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back</span>
                                </a>
                                <span id="btn_save_user" class="btn btn-primary">Submit</span>
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include_once 'footer.php'; ?>
    </div>
    
    <?php include_once 'script.html'; ?>
</body>

</html>