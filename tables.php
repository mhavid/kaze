<?php
    /**
     * 
     * Show All User
     */

    function show_users(){
        $body = '';
        $q = query("SELECT * FROM k_user WHERE k_user.status >=1");
        while($row = mysqli_fetch_array($q)){
            $hash = encrypt('edit_user_id'.$row['user_id']);
            $hash_delete = encrypt('delete_user_id'.$row['user_id']);
            $role = session('role');
            $btn_edit = ($role == 1) 
                ? "<a href='edit_user.php?uid={$row['user_id']}&h=$hash'>
                        <span class='btn btn-xs btn-primary'>
                            <i class='fa fa-edit'></i>
                        </span>
                    </a>"
                : "";
            $btn_delete = ($role == 1)
                ? "<span class='btn btn-xs btn-danger delete_user' uid='{$row['user_id']}' hash='$hash_delete'>
                        <i class='fa fa-trash'></i>
                    </span>"
                : "";

            $role = ($row['role'] == 1) 
                ? "<span class='badge badge-success'>Admin</span>"
                : "<span class='badge badge-danger'>User</span>";
            $status = ($row['status'] == 2) 
                ? "<span class='btn btn-success btn-xs btn_status'>Active</span>"
                : "<span class='btn btn-danger btn-xs btn_status'>Non-Active</span>";
            $body .="
                <tr>
                    <td>{$row['user_id']}</td>
                    <td>$btn_edit $btn_delete</td>
                    <td>{$row['username']}</td>
                    <td>{$row['email']}</td>
                    <td>$role</td>
                    <td>$status</td>
                </tr>
            ";
        }
        $html = "
        <div id='table_user'>
            <table id='example2' class='table table-condensed table-sm datatable'>
                <thead>
                    <tr>
                        <th width='30px'>ID</th>
                        <th>Action</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th width='50px'>Status</th>
                    </tr>
                </thead>
                <tbody>
                    $body
                </tbody>
            </table>
        </div>
        ";

        return $html;
    }
?>