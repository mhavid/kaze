<?php
    /**
     *  Config to the apps
     */
    $config = [
        "database" => [
            "host" => "localhost",
            "port" => "3306",
            "user" => "root",
            "pass" => "",    
            "dbname" => "kaze_test" ,  
        ],
        "debug" => true,
        "secretkey" => "kaze2021",
        "appName" => "Kaze Test"
    ];
?>