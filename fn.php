<?php
    session_start();
    date_default_timezone_set('Asia/Jakarta');

    $mysqli = null;
    /**
     * Set Global Config
     */
    function config($prop) {
        global $config;
        return $config[$prop];
    }

    /** Debug Function */
    function dbg($data){
        echo '<pre>';
            print_r($data);
        echo '</pre>';
    }

    /**
     * Function Connect to Database
     * Error Database
     */
    function connect() {
        global $mysqli;
        if ($mysqli !== null) { return $mysqli; };
        $db = config("database");
        $host = $db['host'];
        $user = $db['user'];
        $pass = $db['pass'];
        $dbname = $db['dbname'];
        $mysqli = mysqli_connect($host, $user, $pass,$dbname);
        return $mysqli;
        
    }
    /**
     * Query no pagging
     * Error Query
     */
    function query($queryStr){
        $mysqli = connect();
        $query = mysqli_query($mysqli, $queryStr);
        $debug = config('debug');
        $data = [];
		if (!$query) {
			$result = [
                "data" => $data,
                "status" => 0,
                "error" => "ERROR: \n" . mysqli_error($mysqli),
                "debug" => ($debug) ? $queryStr : ''
            ];
            dbg($result);
		}
        return $query;
    }

    /**
     * Escape Post Data
     */
    function es($str) {
        $con = connect();
        $nilai = mysqli_real_escape_string($con,$str);
        $nilai = htmlspecialchars($nilai);
        return $nilai;
    }

    /**
     * Function Post Data With Escape Charecter
     */
    function getPost($param,$default='') {
        if(isset($_GET[$param])) {
             return es($_GET[$param]);
        } else
        if(isset($_POST[$param])) {
            return es($_POST[$param]);
        } else {
            return $default;
        }
    }

    /**
     * Encrypt Data With MD5 and Secret Key
     */
    function encrypt($s) {
        return md5($s . config("secretkey"));
    }

    function getIp(){
        if(!empty($_SERVER['HTTP_CLIENT_IP'])){
            //ip from share internet
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
            //ip pass from proxy
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }else{
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
    
    /** function Get User Id */
    function getUserId(){
        if(session('user_id') != '' ) return session('user_id'); 
        echo "Error User Not Found";die;
    }

    /**
     * Function check is login or not
     */
    function isLogin(){
        if(session('user_id') == ''){
            header("location:index.php");die;    
        }
    }

    /**
     * Check Session 
     */
    function session($prop = ''){
        $result = (isset($_SESSION[$prop])) ? $_SESSION[$prop] : '';
        return $result; 
    }

?>