<?php 
  include_once 'config.php';
  include_once 'fn.php';
  include_once 'tables.php';
  isLogin();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <?php include_once 'header.html'; ?>
</head>

<body class="hold-transition sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        <?php include_once 'sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2"></div>
                </div>
                <!-- /.container-fluid -->
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="card">
                    <!-- /.card-header -->
                    <?php include_once 'loading.html'; ?>
                    <div class="card-header">
                        <?php
                            if(session('role') == 1){
                                echo '<a href="new_user.php"><span class="btn btn-primary"><i class="fa fa-plus"></i> New User</span></a>';
                            }
                        ?>
                    </div>
                    <div class="card-body" id="card_dashboard">
                        <?php
                            // </ TO DO HERE >
                            echo show_users();
                        ?>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <?php include_once 'footer.php'; ?>
    </div>
    
    <?php include_once 'script.html'; ?>
</body>

</html>