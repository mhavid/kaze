<?php
    include 'config.php';
    include 'fn.php';
    include 'tables.php';

    /**
     * Time Server is following Jakarta/Bangkok +7:00
     * 
     */
    date_default_timezone_set('Asia/Jakarta');
    
    if (isset($_POST['op'])) {
        $op = $_POST['op'];
        $op();
    }

    /** Save User */
    function save_user(){
        $name = getPost('name');
        $email = getPost('email');
        $password = getPost('password');
        $repassword = getPost('repassword');
        $role = getPost('role');

        /**check if email is in database */
        $q = query("SELECT * from k_user WHERE k_user.email='$email'");
        if(mysqli_num_rows($q) > 0){echo '!! Email is exist'; die;}

        if($password !== $repassword){echo 'Password Not Match'; die;}
        
        /**Encrypt Password */
        $password = encrypt($password.config('secretkey'));

        /**Save to the Database */
        query("INSERT INTO k_user(username, email, password, role) VALUES('$name','$email','$password','$role')");
    }

    /** Update User */
    function update_user(){
        $name = getPost('name');
        $email = getPost('email');
        $role = getPost('role');
        $user_id = getPost('uid');
        $hash = getPost('hash');

        /** Check Hash */
        if(encrypt('edit_user_id'.$user_id) !== $hash){
            echo 'Hash Error'; die;
        }

        /**check if email use in other user */
        $q = query("SELECT * from k_user WHERE k_user.email='$email' and k_user.user_id <>'$user_id'");
        if(mysqli_num_rows($q) > 0){echo '!! Email in use other'; die;}

        /**Update to the Database */
        query("UPDATE k_user SET username='$name', email='$email', role='$role'
         WHERE k_user.user_id='$user_id'");
    }

    //** Delete User */
    function delete_user(){
        $user_id = getPost('uid');
        $hash = getPost('hash');

        /** Check Hash */
        if(encrypt('delete_user_id'.$user_id) !== $hash){
            echo 'error'; die;
        }

        //** Update to database */
        query("UPDATE k_user SET status='0' WHERE k_user.user_id='$user_id'");
        echo show_users();
    }

    //** function Login */
    function login(){
        $email = getPost('email');
        $password = getPost('password');

        $password = encrypt($password.config('secretkey'));

        $q = query("SELECT * from k_user where k_user.email='$email' and k_user.password='$password' and k_user.status ='2'");

        if(mysqli_num_rows($q) == 0){
            echo 'User not found';
            die;
        }
        $row = mysqli_fetch_array($q);
        $_SESSION['user_id'] = $row['user_id'];
        $_SESSION['role'] = $row['role'];
        $_SESSION['username'] = $row['username'];


    }
    
?>